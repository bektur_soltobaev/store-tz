from django.contrib import admin
from .models import Item, ColorOfItem, SizeOfItem, Sort, Basket, Order
from mptt.admin import DraggableMPTTAdmin

admin.site.register(
    Sort,
    DraggableMPTTAdmin,
    list_display=(
        'tree_actions',
        'indented_title',
    ),
    list_display_links=(
        'indented_title',
    ),
)

admin.site.register([Item, ColorOfItem, SizeOfItem, Basket, Order])
