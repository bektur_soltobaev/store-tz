from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import BasketSerializer, ItemSerializer, OrderSerializer, SortSerializer
from .models import Basket, ColorOfItem, Item, Order, SizeOfItem, Sort, AmountCounter


class ItemView(viewsets.ModelViewSet):
    queryset = Item.objects.all()
    lookup_field = 'id'
    serializer_class = ItemSerializer


class SortView(viewsets.ModelViewSet):
    queryset = Sort.objects.all()
    lookup_field = 'id'
    serializer_class = SortSerializer


class BasketView(APIView):

    # noinspection PyMethodMayBeStatic
    def post(self, request):
        serializer = BasketSerializer(data=request.data)
        if serializer.is_valid():
            user = request.user
            session = request.session
            if user.id is not None:
                basket = user.basket
            else:
                session['basket'] = Basket.objects.create().id
                basket = Basket.objects.get(id=session['basket'])
            items = serializer.validated_data['items']
            colors = serializer.validated_data['colors']
            sizes = serializer.validated_data['sizes']

            for item in items:
                obj = Item.objects.get(id=item['product_id'])
                amount = AmountCounter.objects.create(number_to_add=item['amount_to_add'], item=obj)
                amount.save()
                basket.item_basket.add(obj)

            for color in colors:
                obj = ColorOfItem.objects.get(id=color['product_id'])
                amount = AmountCounter.objects.create(number_to_add=color['amount_to_add'], color=obj)
                amount.save()
                basket.color_basket.add(obj)

            for size in sizes:
                obj = SizeOfItem.objects.get(id=size['product_id'])
                amount = AmountCounter.objects.create(number_to_add=size['amount_to_add'], size=obj)
                amount.save()
                basket.size_basket.add(obj)

            return Response({'items in basket': [x.id for x in basket.item_basket.all()],
                             'colors in basket': [x.id for x in basket.color_basket.all()],
                             'size in basket': [x.id for x in basket.size_basket.all()]},
                            status=status.HTTP_200_OK)
        return Response({'detail': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class BasketOutView(APIView):

    def post(self, request):
        serializer = BasketSerializer
        if serializer.is_valid():
            user = request.user
            session = request.session
            if session['basket']:
                basket = Basket.objects.get(id=session['basket'])
            else:
                basket = user.basket
            items = serializer.validated_data['items']
            colors = serializer.validated_data['colors']
            sizes = serializer.validated_data['sizes']

            for item in items:
                obj = basket.item_basket.get(id=item['product_id'])
                obj.amount.number_to_add -= item['amount_to_add']
                obj.quantity += item['amount_to_add']

            for color in colors:
                obj = basket.item_basket.get(id=color['product_id'])
                obj.amount.number_to_add -= color['amount_to_add']
                obj.quantity += color['amount_to_add']

            for size in sizes:
                obj = basket.item_basket.get(id=size['product_id'])
                obj.amount.number_to_add -= size['amount_to_add']
                obj.quantity += size['amount_to_add']

            return Response({'items in basket': [x.id for x in basket.item_basket.all()],
                             'colors in basket': [x.id for x in basket.color_basket.all()],
                             'size in basket': [x.id for x in basket.size_basket.all()]},
                            status=status.HTTP_200_OK)
        return Response({'detail': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class OrderView(APIView):

    # noinspection PyMethodMayBeStatic
    def post(self, request):
        serializer = OrderSerializer(data=request.data)
        if serializer.is_valid():
            user = request.user
            session = request.session
            if user.id is not None:
                basket = user.basket
            else:
                basket = Basket.objects.get(id=session['basket'])
            items = basket.item_basket.all()
            colors = basket.color_basket.all()
            sizes = basket.size_basket.all()
            total_price = 0
            for i in [items, colors, sizes]:
                for j in i:
                    if i == items:
                        j.quantity -= j.amount.number_to_add
                        total_price += j.price * j.amount.number_to_add
                        basket.item_basket.remove(j)
                        j.save()
                    elif i == colors:
                        j.quantity -= j.amount.number_to_add
                        total_price += j.price * j.amount.number_to_add
                        basket.color_basket.remove(j)
                        j.save()
                    else:
                        j.quantity -= j.amount.number_to_add
                        total_price += j.price * j.amount.number_to_add
                        basket.size_basket.remove(j)
                        j.save()
            order = Order.objects.create(delivery=serializer.validated_data['delivery'],
                                         total_price=total_price, basket=basket)
            return Response({'Identifier': order.slug,
                             'Delivery type': order.delivery,
                             'Total price': order.total_price},
                            status=status.HTTP_200_OK)
        return Response({'detail': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
