from rest_framework import serializers
from .models import Sort, Item, ColorOfItem, SizeOfItem, Basket, Order


class ColorOfItemSerializer(serializers.ModelSerializer):
    amount_to_add = serializers.IntegerField(required=False)

    class Meta:
        model = ColorOfItem
        fields = ['id', 'name', 'quantity', 'price', 'amount_to_add']


class SizeOfItemSerializer(serializers.ModelSerializer):
    amount_to_add = serializers.IntegerField(required=False)

    class Meta:
        model = SizeOfItem
        fields = ['id', 'name', 'quantity', 'price', 'amount_to_add']


class ItemSerializer(serializers.ModelSerializer):

    colors = ColorOfItemSerializer(many=True, required=False)
    sizes = SizeOfItemSerializer(many=True, required=False)
    amount_to_add = serializers.IntegerField(required=False)

    class Meta:
        model = Item
        fields = ['id', 'name', 'price', 'colors', 'sizes', 'sort', 'quantity', 'amount_to_add']

    def create(self, validated_data):
        colors_data = validated_data.pop('colors')
        sizes_data = validated_data.pop('sizes')
        sorts_data = validated_data.pop('sort')
        item = Item.objects.create(name=validated_data['name'], price=validated_data['price'],
                                   quantity=validated_data['quantity'])
        if sorts_data:
            item.sort.set(sorts_data)

        if colors_data:
            for i in colors_data:
                color = ColorOfItem.objects.create(name=i['name'], price=i['price'],
                                                   quantity=i['quantity'], item=item)
                color.save()
        if sizes_data:
            for j in sizes_data:
                size = SizeOfItem.objects.create(name=j['name'], price=j['price'],
                                                 quantity=j['quantity'], item=item)
                size.save()
        return item


class SortSerializer(serializers.ModelSerializer):
    items = ItemSerializer(many=True, required=False)

    class Meta:
        model = Sort
        fields = ['id', 'name', 'items', 'parent']


class ItemForBasketSerializer(serializers.Serializer):
        product_id = serializers.IntegerField()
        amount_to_add = serializers.IntegerField()


class BasketSerializer(serializers.ModelSerializer):
    items = ItemForBasketSerializer(many=True, required=False)
    colors = ItemForBasketSerializer(many=True, required=False)
    sizes = ItemForBasketSerializer(many=True, required=False)

    class Meta:
        model = Basket
        fields = ['items', 'sizes', 'colors']


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = ['slug', 'delivery', 'basket']
