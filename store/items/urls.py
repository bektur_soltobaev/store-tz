from django.urls import path
from .views import SortView, ItemView, BasketView, OrderView, BasketOutView

req1 = {'get': 'list', 'post': 'create'}
req2 = {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}

urlpatterns = [
    path('items/', ItemView.as_view(req1), name='items_list_url'),
    path('items/<int:id>/', ItemView.as_view(req2), name='items_detail_url'),
    path('sorts/', SortView.as_view(req1), name='sorts_list_url'),
    path('sorts/<int:id>/', SortView.as_view(req2), name='sorts_detail_url'),
    path('basket/', BasketView.as_view(), name='basket_url'),
    path('basket-out/', BasketOutView.as_view(), name='basket_out_url'),
    path('order/', OrderView.as_view(), name='order_url')
]
