from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model

from .serializers import *


class ItemTest(APITestCase):
    def setUp(self):
        sort1 = Sort.objects.create(name='Okay')
        sort2 = Sort.objects.create(name='deathwing', parent=sort1)
        sort3 = Sort.objects.create(name='lorem', parent=sort2)
        sort4 = Sort.objects.create(name='ipsum', parent=sort3)
        item1 = Item.objects.create(name='item1', price=120, quantity=20)
        item2 = Item.objects.create(name='item2', price=300, quantity=20)
        item1.sort.set([sort1, sort4])
        item2.sort.set([sort2, sort3, sort4])
        color1 = ColorOfItem.objects.create(name='blue', price=150, item=item1, quantity=20)
        size1 = SizeOfItem.objects.create(name='medium', price=170, item=item1, quantity=20)
        self.user = self.setup_user()

    @staticmethod
    def setup_user():
        User = get_user_model()
        return User.objects.create_user(
            username='test',
            email='testuser@test.com',
            password='test'
        )

    def test_create_item(self):
        url = reverse('items_list_url')
        data = {'name': 'item3', 'price': 140, 'quantity': 20,
                'sort': [1, 2],
                'colors': [{'name': 'black', 'price': 500, 'quantity': 20}],
                'sizes': [{'name': 'small', 'price': 140, 'quantity': 15}]}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Item.objects.get(id=3).name, 'item3')
        self.assertEqual(Item.objects.get(id=3).sort.get(id=1).name, 'Okay')
        self.assertEqual(ColorOfItem.objects.get(id=2).price, 500)

    def test_get_single_item(self):
        url = reverse('items_detail_url', args=[2])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_items_list(self):
        url = reverse('items_list_url')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_sort_create(self):
        url = reverse('sorts_list_url')
        data = {'name': 'sort create', 'parent': 1}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Sort.objects.get(id=5).name, 'sort create')

    def test_sort_list(self):
        url = reverse('sorts_list_url')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_single_sort(self):
        url = reverse('sorts_detail_url', args=[1])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data)

    def test_add_to_basket(self):
        url = reverse('basket_url')
        self.client.login(username='test', password='test')
        data = {
            'items': [{
                'product_id': 1,
                'amount_to_add': 15
            }],
            'colors': [{
                'product_id': 1,
                'amount_to_add': 15
            }],
            'sizes': [{
                'product_id': 1,
                'amount_to_add': 10
            }]}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'items in basket': [1], 'colors in basket': [1], 'size in basket': [1]})
        self.assertTrue(ColorOfItem.objects.get(id=1).in_basket.all())
        self.assertTrue(SizeOfItem.objects.get(id=1).in_basket.all())

    def test_make_order(self):
        url = reverse('order_url')
        self.client.login(username='test', password='test')
        data = {'delivery': 2}
        self.client.post(reverse('basket_url'),
                         {
                             'items': [{'product_id': 1, 'amount_to_add': 2}],
                             'colors': [{'product_id': 1, 'amount_to_add': 1}],
                             'sizes': [{'product_id': 1, 'amount_to_add': 1}]},
                         format='json')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'Identifier': 'test', 'Delivery type': 2, 'Total price': 560})
        self.assertEqual(Item.objects.get(id=1).quantity, 18)
        self.assertFalse(Item.objects.get(id=1).in_basket.all())

    def test_add_to_basket_unauthorised(self):
        url = reverse('basket_url')
        data = {'items': [{'product_id': 1, 'amount_to_add': 1}],
                'colors': [{'product_id': 1, 'amount_to_add': 1}],
                'sizes': [{'product_id': 1, 'amount_to_add': 1}]}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'items in basket': [1], 'colors in basket': [1], 'size in basket': [1]})

    def test_make_order_unauthorised(self):
        url = reverse('order_url')
        data = {'delivery': 3}
        self.client.post(reverse('basket_url'),
                         {'items': [{'product_id': 1, 'amount_to_add': 1}],
                          'colors': [{'product_id': 1, 'amount_to_add': 1}],
                          'sizes': []},
                         format='json')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'Identifier': 'anonimoususer', 'Delivery type': 3, 'Total price': 270})
        self.assertEqual(Item.objects.get(id=1).quantity, 19)
        self.assertFalse(Item.objects.get(id=1).in_basket.all())
