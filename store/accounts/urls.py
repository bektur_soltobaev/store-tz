from django.urls import path

from .views import UserView, SignUpAPIView

urlpatterns = [
    path('', UserView.as_view({'get': 'list'}), name='users_list_url'),
    path('<int:id>/', UserView.as_view({'get': 'retrieve', 'put': 'update',
                                         'patch': 'update', 'delete': 'destroy'}), name='users_detail_url'),
    path('signup/', SignUpAPIView.as_view(), name='signup_url')
]
