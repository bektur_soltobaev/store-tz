from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.contrib.auth import get_user_model

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    username = serializers.CharField(max_length=30, required=True)
    password = serializers.CharField(min_length=8, write_only=True, required=True)
    password2 = serializers.CharField(min_length=8, write_only=True, required=True)

    class Meta:
        model = User
        fields = ['id', 'email', 'username', 'password', 'password2']

    def create(self, validated_data):
        if validated_data['password2'] == validated_data['password']:
            user = User.objects.create_user(email=validated_data['email'], password=validated_data['password'],
                                            username=validated_data['username'])
            return user
        raise serializers.ValidationError('Your passwords don\'t match')
