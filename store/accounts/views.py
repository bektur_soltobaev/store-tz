from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models.signals import post_save
from rest_framework import viewsets
from django.dispatch import receiver
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework.authtoken.models import Token

from .serializers import UserSerializer

User = get_user_model()


class SignUpAPIView(APIView):

    permission_classes = (AllowAny,)
    model = User

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                data = serializer.data
                return Response(data, status=status.HTTP_201_CREATED)
        return Response({'detail': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


@receiver(post_save, sender=User)
def create_user_token(sender, instance, created, **kwargs):
    if created:
        Token.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_token(sender, instance, **kwargs):
    instance.auth_token.save()


class UserView(viewsets.ModelViewSet):

    model = User
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (IsAdminUser,)
    lookup_field = 'id'
