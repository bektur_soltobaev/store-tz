from django.urls import reverse
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from rest_framework import status


class SignUpTest(APITestCase):

    def test_sign_up(self):
        url = reverse('signup_url')
        User = get_user_model()
        data = {"username": "test", "email": "test@test.com", "password": "12345678", "password": "12345678"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.get(id=1).username, 'test')
        self.assertTrue(User.objects.get(id=1).auth_token)


    def test_sign_up(self):
        url = reverse('signup_url')
        data = {"username": "test", "email": "test@test.com", "password": "12345678", "password": "test"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UserEndpointsTest(APITestCase):

    def setUp(self):
        self.user = self.setup_user()
        User = get_user_model()
        User.objects.create_user(
            username='test_user_1',
            email='test_user_1@mail.com',
            password='test_user_1'
        )
        User.objects.create_user(
            username='test_user_2',
            email='test_user_2@mail.com',
            password='test_user_2'
        )

    @staticmethod
    def setup_user():
        User = get_user_model()
        return User.objects.create_user(
            username='admin',
            email='admin@test.com',
            password='admin',
            is_staff=True
        )

    def test_users_list(self):
        url = reverse('users_list_url')
        self.client.login(username='admin', password='admin')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data)

    def test_user_detail(self):
        url = reverse('users_detail_url', args=[2])
        self.client.login(username='admin', password='admin')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data)

    def test_token_auth(self):
        User = get_user_model()
        admin = self.user
        user1 = User.objects.get(id=2)
        user2 = User.objects.get(id=3)
        self.assertTrue(admin.auth_token)
        self.assertTrue(user1.auth_token)
        self.assertTrue(user2.auth_token)
